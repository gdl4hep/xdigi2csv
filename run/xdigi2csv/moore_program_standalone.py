"""Moore option file to persist in CSV files the information about

* Clusters or hits in the Velo, UT and SciFi detectors and link to the MC Particle
* MC particle information (pdgid, etc.)
* MC hits
* Event information (number of hits in each detector)

Execute ``dump_csv_simple.py`` trough moore by running

.. code-block::

    /path/to/moore/build/run gaudirun.py input.py dump_csv_simple.py


where ``input.py`` specifies the input file.
"""
import os
from contextlib import ExitStack
from Moore import (
    options,
    run_reconstruction,
    # Difference between run_reconstruction and run_allen_reconstruction?
    # run_allen_reconstruction,
)
from tools import xdigi2csvtools


# CONFIGURE THE VARIABLES HERE =======================================================
# List of detectors for which to dump the hits or clusters
selected_detectors = ["velo", "ut", "scifi"]
# Whether to dump the MC hits (``mchits_{detector}.csv`` files)
dump_mc_hits = False
# Whether to dump ``event_info.csv``
dump_event_info = False
# If set to `False`, we only dump the variables that are necessary for tracking
extended = False
# If set to `True`, all the MC particles in `mc_particles.csv` are dumped, even those
# which don't have any hits
all_mc_particles = False
# Whether to use Retina clusters
retina_clusters = True
# Output directory where the CSV files are stored
outdir = "./jobs/examples/xdgi2csv/output"
# If set to `True`, if a CSV file that is going to be written already exists, it will
# be erase to write the new one
erase = True
# ====================================================================================

# Assert that we dump the hits of at least one detector
assert selected_detectors

# Create output folder
os.makedirs(outdir, exist_ok=True)

# Get list of the names of algorithms to run
run_algonames = xdigi2csvtools.get_algonames(
    detectors=selected_detectors,
    dump_event_info=dump_event_info,
    dump_mc_hits=dump_mc_hits,
)

# Get the reconstruction object
get_reconstruction = xdigi2csvtools.get_algo_sequence(run_algonames)

# Run the algorithms with the correct configuration
with ExitStack() as stack:
    xdigi2csvtools.configure_algos(
        stack,
        algonames=run_algonames,
        outdir=outdir,
        retina_clusters=retina_clusters,
        extended=extended,
        all_mc_particles=all_mc_particles,
        erase=erase,
    )

    run_reconstruction(options, get_reconstruction)
