"""Moore option file to persist in CSV files the information about

* Clusters or hits in the Velo, UT and SciFi detectors and link to the MC Particle
* MC particle information (pdgid, etc.)
* MC hits
* Event information (number of hits in each detector)

**This script is supposed to be run using the ``run.py`` python script to set
the temporary configuration file appropriatly.**
"""
import os
from contextlib import ExitStack
import yaml
from Moore import (
    options,
    run_reconstruction,
    # Difference between run_reconstruction and run_allen_reconstruction?
    # run_allen_reconstruction,
)
from tools import xdigi2csvtools
from tools.envvar import get_environment_variable

# Load the configuration from a YAML file whose path is stored in an environment
# variable.
yaml_path = get_environment_variable("XDIGI2CSV_PROGRAM_CONFIG")
with open(yaml_path, mode='r') as config_file:
    config = yaml.load(config_file, Loader=yaml.SafeLoader)

selected_detectors = config["detectors"]
dump_mc_hits = config["dump_mc_hits"]
dump_event_info = config["dump_event_info"]
extended = config["extended"]
all_mc_particles = config["all_mc_particles"]
retina_clusters = config["retina_clusters"]
outdir = config["outdir"]
erase = config["erase"]

assert isinstance(selected_detectors, list)
assert isinstance(dump_mc_hits, bool)
assert isinstance(dump_event_info, bool)
assert isinstance(extended, bool)
assert isinstance(all_mc_particles, bool)
assert isinstance(outdir, str)
assert isinstance(erase, bool)

# Assert that we dump the hits of at least one detector
assert selected_detectors

# Create output folder
os.makedirs(outdir, exist_ok=True)

# Get list of the names of algorithms to run
run_algonames = xdigi2csvtools.get_algonames(
    detectors=selected_detectors,
    dump_event_info=dump_event_info,
    dump_mc_hits=dump_mc_hits,
)

# Get the reconstruction object that contains the algorithm to run
get_reconstruction = xdigi2csvtools.get_algo_sequence(run_algonames)

# Run the algorithms with the correct configuration
with ExitStack() as stack:
    xdigi2csvtools.configure_algos(
        stack,
        algonames=run_algonames,
        outdir=outdir,
        retina_clusters=retina_clusters,
        extended=extended,
        all_mc_particles=all_mc_particles,
        erase=erase,
    )
    run_reconstruction(options, get_reconstruction)

    # run_allen_reconstruction(options, get_reconstruction)
