"""Moore option file that configures the input file.
"""
import yaml
from tools.envvar import get_environment_variable
from Moore import options

yaml_path = get_environment_variable("XDIGI2CSV_INPUT_CONFIG")
with open(yaml_path, mode='r') as config_file:
    config = yaml.load(config_file, Loader=yaml.SafeLoader)

print("INPUT DATA")
print(yaml.dump(config, sort_keys=False))

options.simulation = config["simulation"]
options.data_type = config["data_type"]
options.conddb_tag = config["conddb_tag"]
options.dddb_tag = config["dddb_tag"]
options.input_type = "ROOT"  # for (X)DIGI file(s)
options.input_files = config["paths"]
options.evt_max = config["evt_max"]

xml_catalog = config.get("xml_catalog")
if xml_catalog:
    from Gaudi.Configuration import (
        Gaudi__MultiFileCatalog as FileCatalog,
        ApplicationMgr,
    )
    catalog = FileCatalog(Catalogs=[f"xmlcatalog_file:{xml_catalog}"])
    ApplicationMgr().ExtSvc.append(catalog)
