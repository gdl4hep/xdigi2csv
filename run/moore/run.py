#!/usr/bin/env python3
"""An helper script that can be used to run one of the moore program
(XDGI2CSV, XDIGI2ROOT or XDIGI2MDF).
The input is defined in the ``moore_input`` section of the YAML file,
and the output directory is defined in the ``output`` section

The helper script has 3 features

1. It can run a function before the main program, which is configured in
:py:mod:`.moore_config` in the :py:data:`.moore_config.program_preruns` dictionary.
2. It runs the main Moore program, whose Moore option file is defined in
``run/{program}/moore_program.py``.
3. It can run a function after the main program
(e.g., to convert a CSV file into a PARQUET file!),
which is configured in :py:mod:`.moore_config`
in the :py:data:`.moore_config.program_postruns` dictionary.

To run the helper script, use the following command:

.. code-block:: bash

    ./run/moore/run.py {program} -c configuration_file.yaml [other arguments]


Replace ``{program}`` with ``xdigi2csv``, ``xdigi2mdf``, or ``xdigi2root``,
depending on which program you want to run.
"""
import os
import tempfile
import yaml
import subprocess
from definitions import dpaths, dconfig
from tools import configmanager, inoutconfig
from tools import envvar
from run.moore import mooreconfig

sections = ["build", "moore_input", "output", "global"]

program_args = dict(
    dest="moore_program", sections=dconfig.moore_programs, help="Moore program to run."
)

constraints = {"build": ["moore", "platform"]}


if __name__ == "__main__":
    # Get the default configuration
    repo_path = envvar.get_repo_path()
    config = configmanager.return_config_from_parser(
        repo=repo_path,
        sections=sections,
        constraints=constraints,
        program_args=program_args,
        parse_known_args=False,
    )

    program = config["moore_program"]

    program_config = config[program]

    # Find the output directory
    outdir = inoutconfig.get_outdir(
        config=config, datatype=dconfig.program_to_datatype.get(program)
    )
    program_config["outdir"] = outdir
    if "dump_log" in program_config:
        dump_log = program_config["dump_log"]
        del program_config["dump_log"]
    else:
        dump_log = False

    # Run pre-run function
    if program in mooreconfig.program_preruns:
        postrun_kwargs = mooreconfig.program_preruns[program](config)
    else:
        assert (
            program not in mooreconfig.program_postruns
        ), "A pre-run function is required to run a post-run function"

    with (
        # This context manager also configures the input
        inoutconfig.MooreInputConfig(
            config=config["moore_input"], repo=repo_path, return_paths=True
        ) as (python_input_to_use, paths, banned_paths),
        # Create the YAML configuration file that contains the configuration
        # of the program
        tempfile.NamedTemporaryFile(
            suffix=".yaml", mode="w"
        ) as program_config_tempfile,
    ):
        nothing_to_run = (not paths) and banned_paths
        if nothing_to_run:
            print("No input.")
        else:
            print(f"CONFIGURATION OF THE {program.upper()} PROGRAM")
            print(yaml.dump(program_config, sort_keys=False))

            yaml.dump(program_config, program_config_tempfile, Dumper=yaml.SafeDumper)
            envvar.set_environment_variable(
                f"{program.upper()}_PROGRAM_CONFIG", program_config_tempfile.name
            )
            # Run the program
            to_run = inoutconfig.get_moore_build(
                config["build"]["moore"], config["build"]["platform"]
            ) + [
                "gaudirun.py",
                python_input_to_use,
                dpaths.moore_program_path.format(repo=repo_path, program=program),
            ]
            print("Run:", " ".join(to_run))
            result = subprocess.run(to_run, cwd=repo_path, env=os.environ)
            counter_fail = 0
            max_fail = 2
            while result.returncode != 0 and counter_fail < max_fail:
                counter_fail += 1
                print(f"Return code is {result.returncode}. Retrying...")
                result = subprocess.run(to_run, cwd=repo_path, env=os.environ)
            del os.environ[f"{program.upper()}_PROGRAM_CONFIG"]

    if not nothing_to_run and result.returncode == 0:
        # Run post-run function
        if program in mooreconfig.program_postruns:
            if postrun_kwargs is not None:
                mooreconfig.program_postruns[program](**postrun_kwargs)

    if dump_log:
        summary = {}

        if paths or banned_paths:
            summary["input"] = paths
            if banned_paths:
                summary["banned"] = banned_paths
        else:
            summary["input"] = python_input_to_use
        if nothing_to_run:
            summary["returncode"] = -1
        else:
            summary["returncode"] = result.returncode
        os.makedirs(outdir, exist_ok=True)
        with open(os.path.join(outdir, "log.yaml"), "w") as summary_file:
            yaml.dump(summary, summary_file, Dumper=yaml.SafeDumper)

    if not nothing_to_run and result.returncode != 0:
        raise Exception("The command failed with error code: " + str(result.returncode))
