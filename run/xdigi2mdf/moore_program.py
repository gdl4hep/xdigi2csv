"""Moore option file to convert a (X)DIGI file into a MDF file.
This file was inspired from the
`mdf_for_standalone_Allen.mdf <https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/RecoConf/options/mdf_for_standalone_Allen.py>`_
option file.

**This script is supposed to be run using the ``run.py`` python script to set
the temporary configuration file appropriatly.**
"""
import os
import yaml
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.mc_checking import tracker_dumper
from RecoConf.hlt1_tracking import (
    make_RetinaCluster_raw_bank,
    make_velo_full_clusters,
    make_RetinaClusters,
)
from PyConf.Algorithms import VPRetinaFullClustering
from Allen.config import allen_non_event_data_config
from RecoConf.hlt1_allen import (
    combine_raw_banks_with_MC_data_for_standalone_Allen_checkers,
)
from tools.envvar import get_environment_variable


yaml_path = get_environment_variable("XDIGI2MDF_PROGRAM_CONFIG")
with open(yaml_path, mode="r") as config_file:
    config = yaml.load(config_file, Loader=yaml.SafeLoader)


# CONFIGURATION =====================================================================
outdir = config["outdir"]
with_retina_clusters = config["retina_clusters"]
mdf_filename = config["mdf_filename"]
geo_dirname = config["geo_dirname"]

#: Path where to save the MDF file
output_mdf_path = os.path.join(outdir, mdf_filename + ".mdf")
#: Path to the directory where to save the binary geometry files
output_geo_dir = os.path.join(outdir, geo_dirname)
# ===================================================================================

# Configure output
options.output_type = "MDF"
options.output_file = output_mdf_path

# Create directories
os.makedirs(outdir, exist_ok=True)
os.makedirs(output_geo_dir, exist_ok=True)
os.makedirs(os.path.dirname(output_mdf_path), exist_ok=True)


# Configure and run algo
def dump_mdf():
    algs = combine_raw_banks_with_MC_data_for_standalone_Allen_checkers(
        output_file=options.output_file
    )
    return Reconstruction("write_mdf", algs)


if with_retina_clusters:
    with allen_non_event_data_config.bind(
        dump_geometry=True, out_dir=output_geo_dir
    ), make_RetinaClusters.bind(
        make_raw=make_RetinaCluster_raw_bank
    ), make_velo_full_clusters.bind(
        make_full_cluster=VPRetinaFullClustering
    ), tracker_dumper.bind(
        velo_hits=make_RetinaClusters
    ):
        run_allen_reconstruction(options, dump_mdf)

else:
    with allen_non_event_data_config.bind(dump_geometry=True, out_dir=output_geo_dir):
        run_allen_reconstruction(options, dump_mdf)
