"""Configure the allen programs.
"""
import copy
from definitions import dallen

#: Associate an allen program name with whether it has an output given by the `output`
#: section.
allen_program_to_has_output = {
    "mdf2csv": True,
    "velo_validation_no_gec": False,
    "seeding_validation_no_gec": False,
    "persistence_velo_tracks": True,
    "persistence_seeding_tracks": True,
    "persistence_seeding_xz_tracks": True,
}


def handle_input_default(program_config: dict) -> dict:
    """Return the input of the Allen program (that will be passed trought
    environment variables) given the program configuration. By default,
    just return a copy of the program configuration.

    Args:
        program_config: program configuration

    Returns:
        A copy of ``program_config``
    """
    return copy.deepcopy(program_config)


def handle_input_mdf2csv(program_config: dict) -> dict:
    """Return the input of the Allen MDF2CSV program."""
    program_input = {}
    for algo in dallen.all_mdf2csv_algos:
        program_config[f"include_{algo}"] = algo in program_config["algos"]
    program_config["erase"] = program_config["erase"]
    return program_input


allen_program_to_handle_input = {
    "mdf2csv": handle_input_mdf2csv,
}
