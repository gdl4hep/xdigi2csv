#!/usr/bin/env python3
"""An helper script to run the MDF2CSV program in Allen, that converts a MDF file
into CSV files. This program utilizes
the ``persistence_csv`` Allen sequence for conversion of MDF files to CSV format.

The input is defined in the ``allen_input`` section of the YAML file,
and the output directory is defined in the ``output`` section.

To run the script, use the following command in the terminal:

.. code-block:: bash

    ./run/mdf2csv/run.py -c configuration_file.yaml [other arguments]

"""
import os
import subprocess
import yaml
from definitions import dconfig, dallen
from tools import configmanager, inoutconfig, envvar, tpaths
from run.allen import allenconfig

sections = ["build", "allen_input", "allen_config", "output", "global"]
constraints = {"build": ["allen_standalone"]}

program_args = dict(
    dest="allen_program", sections=dallen.allen_programs, help="Moore program to run."
)

if __name__ == "__main__":
    # Get the default configuration
    repo_path = envvar.get_repo_path()
    config = configmanager.return_config_from_parser(
        repo=repo_path,
        sections=sections,
        constraints=constraints,
        program_args=program_args,
        parse_known_args=False,
    )
    program = config["allen_program"]
    program_config = config.get(program, {})
    outdir = inoutconfig.get_outdir(
        config=config, datatype=dconfig.program_to_datatype.get(program)
    )

    inpaths, geodir = inoutconfig.get_allen_input(**config["allen_input"])

    # Expand paths with `'*'`
    inpaths = tpaths.expand_paths(inpaths)

    # Get function to handle input
    handle_input = allenconfig.allen_program_to_handle_input.get(
        program, allenconfig.handle_input_default
    )

    program_input = handle_input(program_config=program_config)

    # Add output directory to program input
    if allenconfig.allen_program_to_has_output[program]:
        outdir = inoutconfig.get_outdir(
            config=config, datatype=dconfig.program_to_datatype.get(program)
        )
        program_input["outdir"] = outdir

    # Program configuration is passed using many configuration variables
    # (one could switch to a YAML file)
    for arg_name, arg_value in program_input.items():
        envvar.set_environment_variable(
            f"{program.upper()}_{arg_name.upper()}", arg_value
        )

    print(f"{program.upper()} CONFIGURATION")
    print(yaml.dump(program_input, sort_keys=False))

    # Run the program
    allen_build_dir = config["build"]["allen_standalone"]
    to_run = [
        os.path.join(allen_build_dir, "toolchain", "wrapper"),
        os.path.join(allen_build_dir, "Allen"),
        "--sequence",
        f"{program}",  # sequence name is equal to the allen program name
        "--mdf",
    ] + inpaths
    if geodir:
        to_run.append("-g")
        to_run.append(geodir)

    for allen_param_name, allen_param_value in config["allen_config"].items():
        if allen_param_value is not None:
            to_run += [
                "--" + allen_param_name.replace("_", "-"),
                str(allen_param_value),
            ]

    print("Run:", " ".join(to_run))
    result = subprocess.run(to_run, cwd=allen_build_dir)

    # Delete the environment variables
    for arg_name, arg_value in program_input.items():
        del os.environ[f"{program.upper()}_{arg_name.upper()}"]

    # Exit with error if something went wrong
    if result.returncode != 0:
        raise Exception("The command failed with error code: " + str(result.returncode))
