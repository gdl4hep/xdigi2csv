"""Run an allen sequence on a MDF file.
"""

from __future__ import annotations
from argparse import ArgumentParser

import yaml

from common import write_configuration_to_yaml, run_program


if __name__ == "__main__":
    parser = ArgumentParser("Run an allen sequence on a reference sample.")
    parser.add_argument("-n", "--sample_name", help="Reference sample name.")
    parser.add_argument(
        "-i", "--identifier", help="Identifier of the sample (version, folder number)."
    )
    parser.add_argument("-o", "--outdir", help="Output directory.")
    parser.add_argument(
        "-c",
        "--config",
        help="Configuration file that defines the reference samples.",
    )
    parser.add_argument(
        "-s",
        "--sequence",
        help="Allen sequence to run.",
    )

    parsed_args = parser.parse_args()
    sample_name: str = parsed_args.sample_name
    identifier: str = parsed_args.identifier
    outdir: str = parsed_args.outdir
    config_path: str = parsed_args.config
    allen_sequence: str = parsed_args.sequence

    with open(config_path, "r") as config_file:
        reference_config: dict = yaml.load(config_file, Loader=yaml.SafeLoader)

    allen_sequence_config = {}
    sample_name_idenfier = f"{sample_name}_{identifier}"
    allen_sequence_config["allen_input"] = {"indir": "."}
    allen_sequence_config["output"] = {
        "outdir": allen_sequence,
        "logpath": f"{allen_sequence}.log",
    }
    allen_sequence_config["computing"] = {"program": allen_sequence}
    allen_sequence_config["allen_config"] = reference_config.get("allen_config", {})

    # Write config to YAML file
    job_path = write_configuration_to_yaml(
        config=allen_sequence_config, outdir=outdir, program_name=allen_sequence
    )

    # Run program using this configuration
    run_program(config_path=job_path, program_label=f"{allen_sequence} sequence")
