"""A script to generate the CSV-like files of a reference sample.
"""
from __future__ import annotations
import typing
from argparse import ArgumentParser

import yaml

from common import write_configuration_to_yaml, run_program

if __name__ == "__main__":
    parser = ArgumentParser("Generate the MDF file of a reference sample.")
    parser.add_argument("-n", "--sample_name", help="Reference sample name.")
    parser.add_argument(
        "-i", "--identifier", help="Identifier of the sample (version, folder number)."
    )
    parser.add_argument("-o", "--outdir", help="Output directory.")
    parser.add_argument(
        "-c",
        "--config",
        help="Configuration file that defines the reference samples.",
    )

    parsed_args = parser.parse_args()
    sample_name: str = parsed_args.sample_name
    identifier: str = parsed_args.identifier
    outdir: str = parsed_args.outdir
    config_path: str = parsed_args.config

    with open(config_path, "r") as config_file:
        reference_config: dict = yaml.load(config_file, Loader=yaml.SafeLoader)

    sample_config: dict = reference_config[sample_name]
    paths: str | typing.List[str] = sample_config.pop("paths")[identifier]

    # Configure input
    xdigi2csv_config = {}
    xdigi2csv_config["moore_input"] = {
        "paths": paths,
        **sample_config.get("moore_input", {}),
    }
    # Configure output
    xdigi2csv_config["output"] = {"outdir": "xdigi2csv", "logpath": "xdigi2csv.log"}

    # Configure algorithm
    xdigi2csv_config["computing"] = {"program": "xdigi2csv"}
    xdigi2csv_config["xdigi2csv"] = sample_config.get("xdigi2csv", {})
    xdigi2csv_config["xdigi2csv"]["detectors"] = ["velo", "ut", "scifi"]
    xdigi2csv_config["xdigi2csv"]["format"] = "parquet"
    xdigi2csv_config["xdigi2csv"]["compression"] = "lz4"

    # Configure running
    xdigi2csv_config["build"] = sample_config.get("build", {})

    # Write config to YAML file
    job_path = write_configuration_to_yaml(
        config=xdigi2csv_config, outdir=outdir, program_name="xdigi2csv"
    )

    # Run program using this configuration
    run_program(config_path=job_path, program_label="XDIGI2CSV conversion")
