"""A script to generate a MDF file of a reference sample.
"""
from __future__ import annotations
import typing
from argparse import ArgumentParser

import yaml

from common import write_configuration_to_yaml, run_program

if __name__ == "__main__":
    parser = ArgumentParser("Generate the MDF file of a reference sample.")
    parser.add_argument("-n", "--sample_name", help="Reference sample name.")
    parser.add_argument(
        "-i", "--identifier", help="Identifier of the sample (version, folder number)."
    )
    parser.add_argument("-o", "--outdir", help="Output directory.")
    parser.add_argument(
        "-c",
        "--config",
        help="Configuration file that defines the reference samples.",
    )

    parsed_args = parser.parse_args()
    sample_name: str = parsed_args.sample_name
    identifier: str = parsed_args.identifier
    outdir: str = parsed_args.outdir
    config_path: str = parsed_args.config

    with open(config_path, "r") as config_file:
        reference_config: dict = yaml.load(config_file, Loader=yaml.SafeLoader)

    sample_config: dict = reference_config[sample_name]
    paths: str | typing.List[str] = sample_config.pop("paths")[identifier]

    # Configure input
    xdigi2mdf_config = {}
    xdigi2mdf_config["moore_input"] = {
        "paths": paths,
        **sample_config.get("moore_input", {}),
    }
    # Configure output
    xdigi2mdf_config["output"] = {"outdir": ".", "logpath": "std.log"}

    # Configure algorithm
    xdigi2mdf_config["computing"] = {"program": "xdigi2mdf"}
    if "xdigi2mdf" in sample_config:
        xdigi2mdf_config["xdigi2mdf"] = sample_config["xdigi2mdf"]

    # Configure running
    xdigi2mdf_config["build"] = sample_config.get("build", {})

    # Write config to YAML file
    job_path = write_configuration_to_yaml(
        config=xdigi2mdf_config, outdir=outdir, program_name="xdigi2mdf"
    )

    # Run program using this configuration
    run_program(config_path=job_path, program_label="MDF conversion")
