"""A module that defines common function to treat reference samples.
"""
import typing
import os
import subprocess
import yaml


def write_configuration_to_yaml(
    config: typing.Dict[typing.Any, typing.Any], outdir: str, program_name: str
) -> str:
    os.makedirs(outdir, exist_ok=True)
    job_path = os.path.join(outdir, f"{program_name}.yaml")
    with open(job_path, "w") as job_file:
        yaml.dump(config, job_file, Dumper=yaml.SafeDumper, sort_keys=False)
    print(f"{program_name} configuration written in {job_path}")
    return job_path


def run_program(config_path: str, program_label: str):
    """Run a program using a configuration written in a YAML file."""
    to_run = [os.path.join("run", "run.py"), "-c", config_path]
    print("Run", " ".join(to_run))
    output = subprocess.run(to_run)
    if output.returncode != 0:
        raise Exception(f"{program_label} failed with return code {output.returncode}")
