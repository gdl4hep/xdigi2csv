"""Moore option file to onvert (X)DIGI file to ROOT.

**This script is supposed to be run using the ``run.py`` python script to set
the temporary configuration file appropriatly.**
"""
import yaml
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.mc_checking import tracker_dumper
from RecoConf.hlt1_tracking import (
    make_RetinaCluster_raw_event,
    make_velo_full_clusters,
    make_RetinaDecoder_raw_event,
)
from PyConf.Algorithms import VPRetinaFullClustering
from tools import envvar

yaml_path = envvar.get_environment_variable("XDIGI2ROOT_PROGRAM_CONFIG")
with open(yaml_path, mode='r') as config_file:
    config = yaml.load(config_file, Loader=yaml.SafeLoader)

with_retina_clusters = config["retina_clusters"]
outdir = config["outdir"]

assert isinstance(with_retina_clusters, bool)
assert isinstance(outdir, str)

if with_retina_clusters:
    with (
        make_RetinaDecoder_raw_event.bind(make_raw=make_RetinaCluster_raw_event),
        make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering),
        tracker_dumper.bind(
            velo_hits=make_RetinaDecoder_raw_event,
            root_output_dir=outdir,
            dump_to_root=True)
    ):
        run_allen_reconstruction(
            options,
            lambda: Reconstruction("to_root", [tracker_dumper()])
        )
else:
    run_allen_reconstruction(
        options,
        lambda: Reconstruction("to_root", [tracker_dumper()])
    )
