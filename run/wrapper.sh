#!/bin/bash
# A wrapper that sources the environment and then run your command

# Path of the home in the node
export NODE_HOME=$(pwd);

command=$@
shift
# Remove the line arguments (`lbEnv` seems to see them)
set --
# Source the setup file
echo "source environment"
source $XDIGI2CSV_REPO/setup/setup_remote.sh

# Execute the command to run
echo "Run: $command"
$command