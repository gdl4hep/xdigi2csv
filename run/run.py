#!/usr/bin/env python3
"""Helper python script to run jobs with different backends.

The available backends are
- ``local``: to run a job locally
- ``condor``: to run many jobs in parallel on LXPLUS using HTCondor

The program to run is specified in `computing/program` in the configuration file,
or using the `--program` option.

To execute this script, run the following command:

.. code-block:: bash

    ./run/run.py -c configuration_file.yaml [OPTIONS]

"""
import os
import os.path as op
import contextlib
import subprocess

from tools import envvar, configmanager, inoutconfig, tpaths, tcomputing
from definitions import dconfig, dpaths, dcomputing, dallen


sections = ["computing", "moore_input", "output", "global"]

if __name__ == "__main__":
    repo_path = envvar.get_repo_path()
    # Read configuration
    config, unknown_args = configmanager.return_config_from_parser(
        repo=repo_path, sections=sections, parse_known_args=True
    )
    comp_config = config["computing"]
    del config["computing"]
    program = comp_config["program"]
    assert program in dconfig.programs, (
        str(program) + " is not recognised as a valid program"
    )
    if program in dconfig.moore_programs:
        program_path = dpaths.program_script_path.format(
            repo=repo_path, program="moore"
        )
        args = [program]
    elif program in dallen.allen_programs:
        program_path = dpaths.program_script_path.format(
            repo=repo_path, program="allen"
        )
        args = [program]
    else:
        program_path = dpaths.program_script_path.format(
            repo=repo_path, program=program
        )
        args = []
    args += unknown_args
    input_section = dconfig.program_to_input_section[program]
    backend = comp_config["backend"]
    if backend == "local":  # just run it locally
        config_temp_file = tpaths.write_yaml_temp_file(config, delete=False)
        args += ["-c", config_temp_file.name]
        to_run = [program_path] + args

        print("Run:", " ".join(to_run))

        logpath = config["output"]["logpath"]
        if logpath is not None:
            with contextlib.suppress(FileNotFoundError):
                os.remove(logpath)
            os.makedirs(op.dirname(logpath), exist_ok=True)

            result = subprocess.run(
                to_run, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True
            )
            print(result.stdout)

            with open(logpath, "w") as log_file:
                log_file.write(result.stdout)

            print(f"Log was written in {logpath}")

        else:
            result = subprocess.run(to_run)

        if result.returncode != 0:
            raise Exception(
                "The command failed with error code: " + str(result.returncode)
            )
    else:
        assert backend in dcomputing.backends, (
            str(backend) + " is not recognised as a valid backend"
        )
        # Create subjobs by splitting the inputs
        if input_section == "moore_input":
            # Need to get the output directory to save the log close to the output
            # directory, as well as the configuration of the job
            outdir = inoutconfig.get_outdir(
                config=config, datatype=dconfig.program_to_datatype["xdigi2csv"]
            )
            config_path = op.join(outdir, "job_config.yaml")
            # Process input and get paths
            moore_input_config = inoutconfig.get_moore_input_config(
                config["moore_input"]
            )

            # 2 cases:
            # - `moore_input_config` is `None` which means a python file is used
            #   as an input for Moore.
            #   In this case, no splitting can be done so we run the job without
            #   splitting
            # - `moore_input_config` is not `None` and the value associated with
            #   the key `paths` contains the list of input paths
            if moore_input_config is not None:
                paths = moore_input_config["paths"]
                config["moore_input"] = moore_input_config
                # Splitting
                nb_files = len(paths)
                nb_files_per_job = comp_config["nb_files_per_job"]
                if nb_files_per_job != -1:
                    assert isinstance(nb_files_per_job, int)
                    assert nb_files_per_job > 0
                    config["moore_input"]["nb_files"] = nb_files_per_job

            else:  # python file is used as input
                # There is nothing to do - we run the job as it is
                nb_files_per_job = -1
                nb_files = None

            # Write altered configuration to YAML file (without the paths)
            os.makedirs(outdir, exist_ok=True)
            tpaths.write_yaml_file(config, config_path)

            # Where to save the log
            auto_output_mode = config["output"]["auto_output_mode"]
            if auto_output_mode == "eos":
                # Log cannot be saved in EOS
                # As a workaround, it is save in the `log` folder
                # of this repository (or whatever is set in the configuration file)
                base_logdir = repo_path
                logdir = comp_config["wildcard_data_logdir"].format(
                    repo=repo_path,
                    datatype=dconfig.program_to_datatype[program],
                    version=config["output"]["version"],
                    dataname=config["output"]["dataname"],
                )
            else:
                logdir = op.join(outdir, "log")

            print("Log directory:", logdir)

            os.makedirs(logdir, exist_ok=True)
            args += ["-c", config_path]
            tcomputing.submit_job(
                script_path=program_path,
                args=args,
                nb_files=nb_files,
                nb_files_per_job=nb_files_per_job,
                backend=backend,
                logdir=logdir,
                max_runtime=comp_config["max_runtime"],
                max_transfer_output_mb=comp_config["max_transfer_output_mb"],
            )

        else:
            raise ValueError("Input section " + str(input_section) + " not supported")
