from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.persistence_csv import csv_count_hits

run_allen_reconstruction(
    options,
    lambda: Reconstruction("to_root", [csv_count_hits()])
)
