#!/bin/bash
source /cvmfs/lhcb.cern.ch/lib/LbEnv
current_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source $current_dir/setup.sh
