#!/bin/bash
# path to the ROOT repository
export XDIGI2CSV_REPO="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
# export CSV_EOS_OUTDIR="/eos/lhcb/user/a/anthonyc/tracking/data/csv/"
export PYTHONPATH=$PYTHONPATH:$XDIGI2CSV_REPO

