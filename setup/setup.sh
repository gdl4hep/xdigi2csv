#!/bin/bash
current_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

source $current_dir/setup_minimal.sh

# Source the LHCb environment with `pyarrow` and `pyyaml`
env_path=$current_dir/xdigi2csv_env/bin/activate
env_path_snakemake=$current_dir/xdigi2csv_snakemake_env/bin/activate

if [ -f "$env_path_snakemake" ]; then
    source $env_path_snakemake
    echo "xdigi2csv_snakemake_env environment was sourced."
elif [ -f "$env_path" ]; then
    source $env_path
    echo "xdigi2csv_env environment was sourced."
else
    echo "xdigi2csv_env environment was not found."
fi

export GANGA_CONFIG_PATH=${GANGA_CONFIG_PATH:-GangaLHCb/LHCb.ini}
export GANGA_SITE_CONFIG_AREA=${GANGA_SITE_CONFIG_AREA:-/cvmfs/lhcb.cern.ch/lib/GangaConfig/config}
export PYTHONPATH=$PYTHONPATH:/cvmfs/ganga.cern.ch/Ganga/install/LATEST/lib/python3.8/site-packages/
