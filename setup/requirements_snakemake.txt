pyarrow==11.0.0
PyYAML==6.0
numpy==1.24.2
subprocess-tee==0.4.1
snakemake==7.32.3