#!/bin/bash
# Set-up the environment for the first time

current_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

envdir=$current_dir/xdigi2csv_env

if [ -d "$envdir" ]; then
  # Take action if $DIR exists. #
  echo "The directory $envdir already exists."
  echo "Please remove it to reinstall the environment."
  exit 1
fi

echo "Create the environment (1/3)"
echo "in $envdir"
python3 -m venv $envdir

echo "Source the environment (2/3)"
source $envdir/bin/activate

echo "Install pyarrow and pyyaml (3/3)"
pip install -r $current_dir/requirements.txt
