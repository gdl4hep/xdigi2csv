# CSV Files and Columns

This section describes the CSV files that can be generated using the XDIGI2CSV
algorithm in Moore.


```{note}
Distances are expressed in mm and energy in MeV.
```

8 files can be generated using the Moore algorithms
- `hits_velo.csv`, `hits_ut.csv` and `hits_scifi.csv` contains the **clusters**
in the corresponding tracking detectors, as well as `mcid`, a number
that allows to identify a MC particle.
- `mc_particles.py`: associates an `event` and `mcid` with the characteristics
of the corresponding MC particle (momentum, eta, etc.).
- `mchits_velo.csv`, `mchits_ut.csv` and `mchits_scifi.csv` describes
the position of the MC hits as simulated, before they get digitised
by [Boole](https://gitlab.cern.ch/lhcb/Boole).
- `event_info.csv` contains information about the events. For the moment,
it only contains the number of Velo, UT and SciFi hits for each event,
so it has a limited interest.

```{important}
Despite his name, `hits_velo.csv` contains the velo clusters, not the velo hits.
```

For all CSV files,
- `run` is an integer that identifies a specific run, which is a set of events
- `event` is an integer that identifies a particular event within the run

```{versionadded} 2.1
`run` in all CSV files.
```

## `hits_{detector}.csv` file
- `lhcbid`: index that identifies a decoded hit or cluster
for the Velo, UT, or SciFi detectors.
- `mcid`: identifier that associates a detector hit with a corresponding MC particle.
If the hit is considered a "fake hit," the mcid will be equal to -1.

```{important}
Please note that one hit or cluster can be associated with multiple MC particles.
Therefore, there may be multiple rows in the CSV file that correspond to the same hit,
but for different MC particles (i.e., different `mcid`).
```


### `hits_velo.csv` file
This file contains the velo clusters.
The columns are:
- `event`
- `station`: plane number, between 0 and 25
- `x`, `y` and `z`: the global coordinates of the cluster
- `lhcbid` and `mcid`
- `xfraction` and `yfraction`, the inter-pixel fraction along the
$x$ and $y$ directions
- `sensor`: sensor number, between 0 and 207

```{versionadded} 2.2
`xfraction`, `yfraction` and `sensor` in `hits_velo.csv`.
```


### `hits_ut.csv` file
This file contains the UT hits.
The columns are:
- `event`
- `plane`: plane number, between 0 and 25
- `xatyeq0`: $x$-coordinate at $y = 0$
- `zatyeq0`: $z$-coordinate at $y = 0$
- `dxdy`: represents the slope between the $y$- and $x$-axes,
which is used to calculate the x-coordinate of a hit.
The x-coordinate can be computed by the formula `xatyeq0 + dxdy * yatxeq0`.
- `weight`: Square of the inversed error: $\frac{1}{\text{error}^2}$
- `lhcbid` and `mcid`

By default, `ybegin` and `yend` values, which correspond to the the y-coordinate
of the start and end points of the fibre line,
are not stored, as they provide limited information about the y-position of a particle.
However, it's worth noting that these values can be saved if deemed necessary
for a specific analysis.

### `hits_scifi.csv` file
This file contains the FT hits.
The columns are:
- `event`
- `plane`: plane number, between 0 and 11
- `xatyeq0`: $x$-coordinate at $y = 0$
- `zatyeq0`: $z$-coordinate at $y = 0$
- `dxdy`: represents the slope between the $y$- and $x$-axes
- `dzdy`: represents the slope between the $y$- and $z$-axes
- `weight`: Square of the inversed error: $\frac{1}{\text{error}^2}$
- `lhcbid` and `mcid`

## `mc_particles.csv` file
This file contains information about each MC particle.
The columns are:
- `event`: the event number in which the particle was generated
- `mcid`: the unique identifier of the MC particle
- `pid`: the PDGID (Particle Data Group ID) of the particle
- `p`, `pt`, `eta` and `phi`: the momentum, transverse momentum, pseudorapidity,
and azimuthal angle of the MC particle.
- `charge`: the electric charge of the particle
- `mother_mcid`: the `mcid` of the mother particle. `-1` if no mother particle was found
for this MC particle.
- `origin_mother_mcid`: the `mcid` of the original particle. `-1` if no original particle
was found for this MC particle.
- `has_velo`, `has_ut`, `has_scifi`: whether the MC particle is reconstructible
in the Velo, UT and SciFi, respectively.
I am uncertain about the precise meaning of "reconstructible" it could be possible
this variable only takes into account the acceptance.
- `from_bdecay`, `from_cdecay`, `from_sdecay`: whether the MC particle belongs to a decay chain
with a $b$-quark, $c$-quark or $s$-quark hadron.
- `nhits_velo`, `nhits_ut`, `nhits_scifi`: number of hits in the Velo, UT and SciFi
of this MC Particle.
- `vx`, `vy`, `vz`: coordinates of the origin vertex
- `nhits_velo`, `nhits_ut`, `nhits_scifi`: number of hits in the velo, UT and SciFi

```{versionadded} 2.0
`nhits_velo`, `nhits_ut`, `nhits_scifi` in `mc_particles.csv`.
```

## `mchits_{detector}.csv` file
The file contains information about the MC hits of the MC particles.
The columns are:
- `event`: the event number in which the particle was generated
- `inx`, `iny`, `inz`: coordinates of the entry point to a detector plane
- `outx`, `outy`, `outz`: coordinates of the exit point from the detector plane
- `time`: time relative to the origin collision
- `mcid`: the unique identifier of the MC particle this MC hit belongs to

## `event_info.csv` file
The file contains information about an event.
The columns are
- `event`
- `n_velo_hits`, `n_ut_hits`, `n_scifi_hits`: the number of hits in the Velo, UT,
and SciFi detectors, respectively.
