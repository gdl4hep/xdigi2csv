# Programs

This page describes the programs that can be executed through this repository.

## 4 Programs

The repository includes 4 programs:
- 3 Moore programs, run using the LHCb stack, that allow to convert (X)DIGI files
into CSV files (`XDIGI2MDF`), ROOT files (`XDIGI2ROOT`) and MDF (`XDIGI2MDF`).
- 1 Allen program, `MDF2CSV`, that allows to convert a MDF file into CSV files

```{danger}
The Allen MDF2CSV program is currently available, but it has not been extensively
tested. Its functionality is being maintained in case it is needed in the future.
```

```{note}
Please note that of all the programs in this repository, the XDIGI2CSV program
is the only one that can be safely run using HTCONDOR.
```

The programs and their corresponding helper scripts are summarised in the table
below.

| Program | Conversion | HTCONDOR supported | Helper script |
| ------- | ---------- | ------------------ | ------------- |
| XDIGI2CSV | (X)DIGI $\to$ CSV or parquet  | YES | `./run/moore/run.py xdigi2csv` |
| XDIGI2ROOT | (X)DIGI $\to$ ROOT | NO | `./run/moore/run.py xdigi2root` |
| XDIGI2MDF | (X)DIGI $\to$ MDF | NO | `./run/moore/run.py xdigi2mdf` |
| MDF2CSV | MDF $\to$ CSV | NO | `./run/mdf2csv/run.py` |

The programs run algorithms in Moore or Allen. The repositories and algorithms
are summarised in the table below.

| Program | Build | Algorithms |
| ------- | ----- | ---------- |
| XDIGI2CSV | Moore in LHCb stack <br> `anthonyc-persistence_csv` branches | [`Allen/Dumpers/CSVDumpers`](https://gitlab.cern.ch/lhcb/Allen/-/tree/anthonyc-persistence_csv/Dumpers/CSVDumpers) |
| XDIGI2ROOT | Moore in LHCb stack | [`Allen/Dumpers/ROOTDumpers`](https://gitlab.cern.ch/lhcb/Allen/-/tree/anthonyc-persistence_csv/Dumpers/RootDumpers) |
| XDIGI2MDF | Moore in LHCb stack | |
| MDF2CSV | Allen <br> `anthonyc-persistence_csv_mdf` branch | [`Allen/host/persistence`](https://gitlab.cern.ch/lhcb/Allen/-/tree/anthonyc-persistence_csv_mdf/host/persistence) |

I am the author of the XDIGI2CSV and MDF2CSV programs. The other Moore programs,
XDIGI2MDF and XDIGI2ROOT, can be run in the `master` branches, or using the
build versions in CVMFS.

The XDIGI2CSV, XDIGI2ROOT, and XDIGI2MDF programs are referred to as "Moore programs",
and are intended to be run from Moore.

## How to Run a Program

You can run any of these programs using the central `run/run.py` helper script
as follows:
```bash
./run/run.py --program {program} [other arguments]
```
Replace `{program}` by `xdigi2csv`, `xdigi2root`, `xdigi2mdf`, or `mdf2csv`, depending
on the program you want to run. I advice to use this script rather than
the others, as you can only submit jobs to HTCONDOR using the latter.
**It is recommended to use this script instead of the other individual scripts,
as only this one supports job submission to HTCONDOR.**

When you run the `run/run.py` script, it will automatically execute
the appropriate program script: `./run/moore/run.py` or `./run/mdf2csv/run.py`,
depending on the program you specified.

If you need detailed instructions on how to run any of these programs,
please refer to the following pages
- [XDIGI2CSV program](4.xdigi2csv_program.md): for instructions on how to run
the XDIGI2CSV program
- [Other programs](5.other_programs.md): for instructions on how to run
the other Moore programs XDIGI2MDF and XDIGI2ROOT
- [MDF2CSV program](../More/3.mdf2csv_program.md): for instructions on how to run
the MDF2CSV program, as well as details on the outputted CSV files.

Before running any of these programs, remember to `source setup/setup.sh`
as explained in the [Setup Guide](1.setup.md).

## XDIGI2CSV vs MDF2CSV

Whether using XDIGI or MDF as input, it is possible to obtain CSV-like files
using either the Moore XDIGI2CSV or Allen MDF2CSV solutions. 
However, there are some differences to consider when choosing between them:

- The Moore XDIGI2CSV solution is preferred if you want to process XDIGI files
without downloading them to your local machine, as it can work with grid links as inputs.
Additionally, XDIGI files contain MC hits, which can be dumped if desired,
whereas Allen does not read MC hits.
- The Allen MDF2CSV solution is faster and offers built-in decoding algorithms,
which can be beneficial if you want to develop algorithms for Allen.

