# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "DIGI2CSV"
copyright = "2023, Anthony Correia"
author = "Anthony Correia"
release = "v2.1"

master_doc = "index"
language = "en"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",  # aut-documentation of python code
    "sphinx.ext.viewcode",  # be able to see code directly in the documentation website
    "sphinx.ext.napoleon",  # numpy doc style
    # "sphinx.ext.coverage",
    "sphinx_autodoc_typehints",  # to read the python typehints
    "sphinx.ext.mathjax",  # for math support
    "sphinx-mathjax-offline",  # for using mathjax in offline mode
    "myst_parser",  # to parse markdown files
    "sphinx_multiversion",
]

autoapi_dirs = [".."]

exclude_patterns = []

# The list of the python modules and packages not to import
autodoc_mock_imports = ["Moore", "RecoConf", "PyConf"]


source_suffix = [".rst", ".md"]
myst_enable_extensions = [
    "dollarmath",
    "amsmath",
    "tasklist",
]
# myst_dmath_double_inline = True
myst_heading_anchors = 3
myst_number_code_blocks = ["typescript"]
myst_update_mathjax = False

# Whitelist pattern for branches (set to '' to ignore all branches)
smv_branch_whitelist = "^(master)$"  # Only master
smv_released_pattern = r"v*"
smv_latest_version = "v2.4"
smv_tag_whitelist = r'^(v.*)$'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_book_theme"
html_static_path = ["_static"]
html_theme_options = {
    "repository_url": "https://gitlab.cern.ch/gdl4hep/xdigi2csv",
    "use_repository_button": True,
    "path_to_docs": "docs/source",
    "use_issues_button": True,
    "use_edit_page_button": False,
    "home_page_in_toc": True,
    "repository_branch": True,
}

html_title = ""
html_logo = "_static/png/logo.png"
html_favicon = "_static/png/logo_squared.png"

if (
    "SPHINX_MULTIVERSION_ACTIVATED" in os.environ
    and os.environ["SPHINX_MULTIVERSION_ACTIVATED"] == "true"
):
    templates_path = [
        "_templates",
    ]


html_css_files = [
    "css/readthedocs-doc-embed.css",
    "css/local.css",
    "css/badge_only.css",
]
