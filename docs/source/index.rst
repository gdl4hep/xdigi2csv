(X)DIGI2CSV
===========

The `(X)DIGI2CSV repository <https://gitlab.cern.ch/gdl4hep/xdigi2csv>`_
is a versatile tool for running Allen and Moore algorithms in a reproducible manner.
While its primary function is to convert (X)DIGI files on the grid
into CSV or PARQUET formats, it can also be used for a variety of other tasks.
For example, it can convert (X)DIGI files into MDF or ROOT, and MDF files into CSV.
The repository also makes it easy to process large sets of files in parallel
by quickly submitting jobs to HTCONDOR.

Every program can be configured using a YAML file such as the one below,
which can be run simply by using `./run/run.py -c the_configuration_file.yaml`.

.. code-block:: yaml

   moore_input:
      nb_files: 3
      start_index: 0
      # Input file is in the grid
      bookkeeping_path: "/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/30000000/XDIGI"
      dddb_tag: "dddb-20210617"
      conddb_tag: "sim-20210617-vc-md100"
      evt_max: -1
   xdigi2csv:  
      detectors: # detector hits or clusters to dump
      - velo
      - ut
      - scifi
      # Extension and compression
      format: parquet
      compression: lz4
   output:
      outdir: "minbias-sim10aU1" # output directory of the CSV files
   computing:
      program: xdigi2csv # program to run


This allows you to define jobs in a way that is quick, easy, and reproducible.
By using the YAML configuration file, you can easily configure the program
to suit your needs, whether you need to convert (X)DIGI files,
generate MDF or ROOT files, or process large sets of files in parallel.

(X)DIGI2CSV is being developed as part of the ANN4EUROPE project, with the goal
of training neural networks for tracking at LHCb.

.. DANGER::
   Please note that the (X)DIGI2CSV repository was developed primarily for internal use
   and may have limitations. We do not recommend its use by external parties.


.. toctree::
   :maxdepth: 3
   :hidden:

   organisation


.. toctree::
   :caption: Accessing Generated Files
   :glob:
   :hidden:

   Access/*


.. toctree::
   :caption: Repository Setup and Usage
   :glob:
   :hidden:

   Guide/*


.. toctree::
   :caption: Additional Information
   :maxdepth: 3
   :glob:
   :hidden:

   More/*


.. toctree::
   :caption: API reference
   :maxdepth: 3
   :hidden:
   :glob:

   apidoc/definitions
   apidoc/tools
   apidoc/run


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
