import os.path as op
from snakefiles import sconfig

print("CONFIGURATION")
print("Data directory used as input:", sconfig.data_outdir_wildcard_in)
print("Data directory used as output:", sconfig.data_outdir_wildcard_out)


rule check_log_wildcard:
    input:
        script=op.join("python", "analysis", "check_log_sanity.py"),
        indir=op.join(sconfig.data_outdir_wildcard_in),
    output:
        outpath=op.join(sconfig.data_outdir_wildcard_out, "analysis", "log_status.yaml"),
    shell:
        "{input.script} -i {input.indir} -o {output.outpath}"

rule count_nb_events_wildcard:
    input:
        script=op.join("python", "analysis", "count_nevents.py"),
        indir=op.join(sconfig.data_outdir_wildcard_in),
    output:
        outpath=op.join(sconfig.data_outdir_wildcard_out, "analysis", "nevents.yaml"),
        plotpath=op.join(
            sconfig.data_outdir_wildcard_out, "analysis", "nevents_per_file.pdf"
        )
    params:
        format=lambda wildcards: sconfig.get_data_config(
            wildcards.dataname
        )["xdigi2csv"]["format"],
        compression=lambda wildcards: sconfig.get_data_config(
            wildcards.dataname
        )["xdigi2csv"]["compression"],
    shell:
        "{input.script} "
        "-i {input.indir} "
        "-o {output.outpath} "
        "-p {output.plotpath} "
        "-f {params.format} "
        "-c {params.compression} "


rule check_log_all:
    input:
        sconfig.expand_all_datanames(rules.check_log_wildcard.output.outpath),

rule count_nevents_all:
    input:
        sconfig.expand_all_datanames(rules.count_nb_events_wildcard.output.outpath),
        sconfig.expand_all_datanames(rules.count_nb_events_wildcard.output.plotpath),

rule all:
    input:
        rules.check_log_all.input,
        rules.count_nevents_all.input,

include: "snakefiles/singular.snake"
include: "snakefiles/reference.snake"