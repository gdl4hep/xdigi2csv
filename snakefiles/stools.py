"""Module that contains helper functions for Snakemake
"""
from string import Formatter


class PartialFormatter(Formatter):
    """Class used for not replacing all the placeholders in a string.
    Taken from
    https://stackoverflow.com/questions/17215400/format-string-unused-named-arguments


    Examples:
        >>> fmt=MyFormatter()
        >>> fmt.format("{a}{b}", a="blabla")
        'blabla{b}'
    """

    def __init__(self, default="{{{0}}}"):
        self.default = default

    def get_value(self, key, args, kwargs):
        if isinstance(key, str):
            return kwargs.get(key, self.default.format(key))
        else:
            return Formatter.get_value(key, args, kwargs)


partialFormatter = PartialFormatter()


def partial_format(*args, **kwargs):
    """Equivalent to the :py:func:`string.format` method,
    which does not raise an error if not all the placeholders are of a string are
    replaced.

    Examples:
        >>> partial_format("{a}{b}", a="blabla")
        'blabla{b}'
    """
    return partialFormatter.format(*args, **kwargs)
