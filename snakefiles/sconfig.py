"""Load the configuration
"""
import typing
import os.path as op
from tools import configmanager, envvar
from snakefiles.stools import partial_format

#: Path to the repository
repo_path = envvar.get_repo_path()

# Load configuation
config = configmanager.import_default_config(
    repo=repo_path,
    sections=["snakemake", "output"],
    constraints={"output": ["wildcard_data_outdir"]},
)

# Read configuration
datadir_in = config["snakemake"]["datadir_in"]
datadir_out = config["snakemake"]["datadir_out"]
data_outdir_wildcard = config["output"]["wildcard_data_outdir"]

# For the moment we only consider one ``datatype``
datatype = "csv"

#: Data directory with placeholders ``version`` and ``dataname``, for input
#: (for fast access, )
data_outdir_wildcard_in = partial_format(
    data_outdir_wildcard, datadir=datadir_in, datatype=datatype
)

#: Data directory with placeholders ``version`` and ``dataname``, for output
data_outdir_wildcard_out = partial_format(
    data_outdir_wildcard, datadir=datadir_out, datatype=datatype
)

#: List of ``dataname`` per ``version``
datanames_per_version = {
    "v2": [
        "minbias-sim10aU1-xdigi",
        "minbias-sim10b-xdigi",
        "minbias-sim10b-xdigi-part2",
        "smog2-digi",
    ],
    "v2.1": ["minbias-sim10b-xdigi-nospillover"],
    "v2.2.2": ["bu2kstee-sim10aU1-xdigi"],
}


def expand_all_datanames(path_wildcard: str) -> typing.List[str]:
    """Expand a path with placeholders ``{version}``, ``{datatype}`` and ``{dataname}``
    with all the possible paths.

    Args:
        path_wildcard: path with placeholders ``{version}``, ``{datatype}``
            and ``{dataname}``

    Returns:
        list of all possible paths given all the versions, datatypes and datanames
        considered.
    """
    return [
        path_wildcard.format(version=version, dataname=dataname)
        for version, datanames in datanames_per_version.items()
        for dataname in datanames
    ]


def get_data_config(dataname, job_config_dir=op.join(repo_path, "jobs", "eos")) -> dict:
    """Get the configuration of a production

    Args:
        dataname: name of the production
        job_condig_dir: directory where all the configurations of the production are

    Returns:
        configuration of ``dataname``
    """
    return configmanager.import_config(op.join(job_config_dir, dataname + ".yaml"))
