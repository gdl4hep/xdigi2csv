from Moore import options
from Gaudi.Configuration import (
    Gaudi__MultiFileCatalog as FileCatalog,
    ApplicationMgr,
)

options.simulation = True
options.data_type = "Upgrade"
options.conddb_tag = "sim-20220929-vc-md100"
options.dddb_tag = "dddb-20221004"
options.input_type = "ROOT"
options.input_files = [
    "LFN:/lhcb/MC/Upgrade/XDIGI/00171960/0000/00171960_00000353_1.xdigi"
]
options.evt_max = 100

xml_file_name = "jobs/examples/xdigi2csv/pool_xml_catalog.xml"
catalog = FileCatalog(Catalogs=[f"xmlcatalog_file:{xml_file_name}"])
ApplicationMgr().ExtSvc.append(catalog)
