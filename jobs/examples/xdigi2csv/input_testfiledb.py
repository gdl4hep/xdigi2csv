"""An example of input option python file for `Moore`.
"""
from Moore import options

options.set_input_and_conds_from_testfiledb(
    'upgrade-minbias-magdown-scifi-v5_retinacluster')

options.evt_max = 100
