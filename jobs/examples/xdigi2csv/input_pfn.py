from Moore import options

options.simulation = True
options.data_type = "Upgrade"
options.conddb_tag = "sim-20220929-vc-md100"
options.dddb_tag = "dddb-20221004"
options.input_type = "ROOT"
options.input_files = [
    "root://gridproxy@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00171960/0000/00171960_00000353_1.xdigi"
]
options.evt_max = 100
