# ROOT vs CSV

I used these YAML files to generate CSV and ROOT files and compare their
reading time.

To download the original XDIGI file that was used as input of the XDIGI2CSV
and XDIGI2ROOT algorithms, you need to run:

```bash
lb-dirac dirac-dms-get-file LFN:/lhcb/MC/Upgrade/XDIGI/00171960/0000/00171960_00000353_1.xdigi
```
