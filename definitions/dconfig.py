"""Define the configuration parameters."""
from . import dgen, dconversion, dallen


#: List of information needed to create the input file, that need to be stored
#: in the configuration file
input_config_params = [
    "dddb_tag",
    "conddb_tag",
    "simulation",
    "data_type",
    "evt_max",
]

#: List of Moore programs, run with ``./run/moore/run.py``
moore_programs = [
    "xdigi2csv",
    "xdigi2mdf",
    "xdigi2root",
    "test",
]

#: List of programs in this repository
programs = moore_programs + dallen.allen_programs

#: Associates a program of this repo with the relevant input section
#: in the configuration
program_to_input_section = {}
for program_ in moore_programs:
    program_to_input_section[program_] = "moore_input"

for program_ in dallen.allen_programs:
    program_to_input_section[program_] = "allen_input"


#: Associates a program with the value of the variable ``datatype``
#: which is used in the path of the output directory if saved in the EOS
program_to_datatype = {
    "xdigi2csv": "csv",
    "xdigi2mdf": "mdf",
    "mdf2csv": "csv",
    "xdigi2root": "root",
}

#: A list of elements that indicate the value that correspond to a path or
#: a list of paths, that could be relative and need to be resolved.
path_list_location = [
    {"moore_input": ["paths"]},
    {"allen_input": ["indir", "paths", "geodir"]},
    {"output": ["outdir", "logpath"]},
    {"build": ["moore", "allen_standalone"]},
    "include",
    {"snakemake": ["datadir_in", "datadir_out"]},
]

#: Associates a section in the configuration with a description
config_sections = {
    "build": "Path to the build directories of Moore and Allen standalone",
    "xdigi2csv": "Conversion from (X)DIGI to CSV",
    "moore_input": (
        "Input of a Moore program. It is either provided by a python file "
        "(`python_input`), a list of PFNs (`paths`), or a unique bookkeeping path "
        "(`bookkeeping_path`) together with `start_index`, `nb_files`, `dddb_tag`, "
        "`conddb_tag`, `simulation`, `data_type` and `evt_max`"
    ),
    "output": (
        "Output directory. Either you can set ``outdir``, or use ``auto_output_mode`` "
        "(in this case, ``outdir`` is overwritten)"
    ),
    "global": "Global variables",
    "computing": "Configuration for running jobs",
    "allen_input": "Input of an Allen program",
    "xdigi2root": "Conversion from (X)DIGI to ROOT",
    "xdigi2mdf": "Conversion from (X)DIGI to MDF",
    "test": "Test program for Moore",
    "mdf2csv": "Conversion from MDF to CSV in Allen (experimental)",
    "velo_validation_no_gec": (
        "Allen velo validation sequence, without the General Event Cut (GEC)"
    ),
    "seeding_validation_no_gec": (
        "Allen seeding validation sequence, without the General Event Cut (GEC)"
    ),
    "persistence_velo_tracks": (
        "Allen sequence to dump the velo tracks into a CSV file."
    ),
    "persistence_seeding_tracks": (
        "Allen sequence to dump the seeding tracks into a CSV file."
    ),
    "persistence_seeding_xz_tracks": (
        "Allen sequence to dump the seeding XZ tracks into a CSV file."
    ),
    "snakemake": "Configuration of the scripts run using snakemakes",
    "allen_config": "Allen configuration",
}

#: Associates a section in the configuration with another dictionnary
#: that associates an option with a dictionnary that will be passed to
#: :py:func:`argparse.ArgumentParser.add_argument`. Use ``nargs="+"`` for lists
#: and ``type=bool`` for a boolean variable
config_description = {
    "build": {
        "moore": dict(help="Path to the Moore repository within the stack"),
        "platform": dict(
            help=("Platform of the build to use. This is used only in a Moore program")
        ),
        "allen_standalone": dict(
            help="Path to the build directory of Allen standalone"
        ),
    },
    "xdigi2csv": {
        "detectors": dict(
            nargs="+",
            help="List of detectors for which the hits and MC particles are dumped",
            choices=dgen.all_detectors,
        ),
        "dump_event_info": dict(type=bool, help="whether to dump `event_info.csv`"),
        "dump_mc_hits": dict(
            type=bool,
            help=(
                "whether to dump the MC hits, i.e.,  `mchits_{detector}.csv` "
                "for the selected detectors"
            ),
        ),
        "all_mc_particles": dict(
            type=bool,
            help=(
                "whether to dump all MC particles, "
                "even those which do not have hits in the selected detectors"
            ),
        ),
        "retina_clusters": dict(
            type=bool, help="Whether retina clusters are in the (X)DIGI file"
        ),
        "extended": dict(
            type=bool,
            help=(
                "Whether to dump more information for the `hits_{detector}.csv` "
                "and `mc_particles.csv`"
            ),
        ),
        "erase": dict(type=bool, help="Whether to erase an existing CSV file"),
        "dump_log": dict(
            type=bool,
            help=(
                "Decide on saving a YAML file in the output folder "
                "that details the input and records the success of the program."
            ),
        ),
        "format": dict(help="Output format.", choices=dconversion.formats),
        "compression": dict(
            help=(
                "Compression algorithm to use. Availability depends on the chosen "
                "format."
            )
        ),
        "keep_original": dict(
            type=bool,
            help=(
                "In the case were the file is compressed or his format is changed, "
                "whether to keep the original."
            ),
        ),
        "intermediate_outdir": dict(
            help=(
                "Intermediate output dir²ectory where to store the CSV file "
                "before the conversion. "
                "You can set this to `.` "
                "No value means that the intermediate output directory is the same as "
                "the output directory."
            )
        ),
        "_intermediate_outdir_in_node": dict(
            type=bool,
            help=(
                "If set to `True`, a `None` value of `intermediate_outdir` "
                "with `keep_original` set to `False` is interpreted as writting "
                "on the local space of condor. "
                "This variable is set by the `run/run.py` script "
                "but you can force it to a value (`true` or `false`)."
            ),
        ),
    },
    "allen_input": {
        "indir": dict(
            help=(
                "Input directory where the MDF files and possibly the geometry "
                "directory are"
            )
        ),
        "mdf_filename": dict(help="If `indir` is used, MDF file name (can be `*.mdf`)"),
        "geo_dirname": dict(help="Geometry directory name inside `indir`"),
        "paths": dict(help="MDF input files for Allen", nargs="+"),
        "geodir": dict(help="Directory where the binary geometry files are for Allen"),
    },
    "moore_input": {
        "python_input": dict(
            help=(
                "Path to the python file that defines the input. "
                "It is handled by Moore"
            )
        ),
        "bookkeeping_path": dict(
            help="Bookkeeping path. The actual PFNs will be determined automatically"
        ),
        "paths": dict(help="Path to the files, directly", nargs="+"),
        "start_index": dict(type=int, help="Index of the file from which to start"),
        "nb_files": dict(
            type=int, help="Number of files to process. -1 for all the files."
        ),
        "dddb_tag": dict(
            help=(
                "Version of the Detector Description DataBase, which specifies "
                "the geometry and material of the LHCb detector"
            )
        ),
        "conddb_tag": dict(
            help=(
                "Version of the conditions database to be used, which specifies "
                "the calibration and alignment constants of the LHCb detector."
            )
        ),
        "simulation": dict(
            help=("Whether the sample is a simulation. ``Upgrade`` for the run 3.")
        ),
        "data_type": dict(
            help=("Data-taking year the sample is supposed to represent.")
        ),
        "evt_max": dict(type=int, help=("Maximal number of events.")),
        "banned_storage_elements": dict(
            nargs="+",
            help="List of storage elements to ban",
        ),
    },
    "output": {
        "outdir": dict(help="Path to the output directory"),
        "auto_output_mode": dict(
            help=(
                "`outdir` if `outdir` is used, `eos` if `wildcard_data_outdir` is used"
            )
        ),
        "wildcard_data_outdir": dict(
            help=(
                "Output paths, with placeholders `version`, `dataname` and `datatype`. "
                "`datatype` would be `csv` for CSV outputs"
            )
        ),
        "version": dict(help="Software version"),
        "dataname": dict(help="Input data name"),
        "suboutdir": dict(help="Sub-directory where to save the files"),
        "logpath": dict(
            help="If set, the stdout and stderr will be written to this file."
        ),
    },
    "global": {"datadir": dict(help="Path where data is stored in the EOS")},
    "computing": {
        "program": dict(help="Which program to run.", choices=programs),
        "nb_files_per_job": dict(help="Number of input files per job."),
        "backend": dict(
            help=(
                "Where to run the job. Use `local` for tests or small jobs "
                "and `condor` for many jobs."
            ),
            choices=["local", "condor"],
        ),
        "wildcard_data_logdir": dict(
            help=(
                "Where to save the log if output is in EOS, that is, "
                "`auto_output_mode` is set to `eos`. "
                "This argument is only relevant if HTCondor is used. "
                "As the log cannot be written on EOS, another directory must be "
                "chosen."
            )
        ),
        "max_runtime": dict(
            type=int,
            help="Maximal time in sec within which the job is expected to run.",
        ),
        "max_transfer_output_mb": dict(
            type=int, help="`MAX_TRANSFER_OUTPUT_MB` HTCondor parameter."
        ),
    },
}

config_description["xdigi2root"] = {
    "retina_clusters": config_description["xdigi2csv"]["retina_clusters"]
}
config_description["xdigi2mdf"] = {
    "retina_clusters": config_description["xdigi2csv"]["retina_clusters"],
    "mdf_filename": dict(help="File name of the outputted MDF file"),
    "geo_dirname": dict(
        help="Directory name of directory where the binary geometry files are dumped"
    ),
}
config_description["mdf2csv"] = {
    "algos": dict(
        help="List of the Allen algorithms to run",
        nargs="+",
        choices=dallen.all_mdf2csv_algos,
    ),
    "erase": config_description["xdigi2csv"]["erase"],
}

config_description["test"] = {}

config_description["snakemake"] = {
    "datadir_in": dict(
        help="Input data directory to use as input for the scripts (need fast access)"
    ),
    "datadir_out": dict(help="Input data directory to use as output for the scripts"),
}

config_description["allen_config"] = {
    "number_of_events": dict(
        help="Number of events to process.",
        type=int,
    ),
    "number_of_slices": dict(
        help="Number of input slices to allocate",
        type=int,
    ),
    "events_per_slice": dict(
        help="Number of events per slice.",
        type=int,
    ),
    "treads": dict(
        help="Number of threads.",
        type=int,
    ),
    "repetitions": dict(
        help="number of threads / streams.",
        type=int,
    ),
    "memory": dict(
        help="Memory to reserve on the device per thread / stream (megabytes)",
        type=int,
    ),
}
