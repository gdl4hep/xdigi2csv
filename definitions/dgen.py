"""Module that defines general variables."""

#: Tracking detectors
all_detectors = ["velo", "ut", "scifi"]

#: All algorithm names (and CSV output names)
all_algonames = (
    ["mc_particles", "event_info"]
    + [f"hits_{detector}" for detector in all_detectors]
    + [f"mchits_{detector}"for detector in all_detectors]
)
