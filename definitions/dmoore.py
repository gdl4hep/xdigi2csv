"""Define the algorithms to use in Moore.
"""
from RecoConf.persistence_csv import (
    csv_persistence_vp_mchits,
    csv_persistence_ut_mchits,
    csv_persistence_ft_mchits,
    csv_persistence_event_info,
    csv_persistence_mc_particles,
    csv_persistence_vp_hits,
    csv_persistence_ut_hits,
    csv_persistence_ft_hits,
)
#: Associates an algorithm name with the actual configurable function in Moore
algoname_to_algo = {
    "hits_velo": csv_persistence_vp_hits,
    "hits_ut": csv_persistence_ut_hits,
    "hits_scifi": csv_persistence_ft_hits,
    "mchits_velo": csv_persistence_vp_mchits,
    "mchits_ut": csv_persistence_ut_mchits,
    "mchits_scifi": csv_persistence_ft_mchits,
    "mc_particles": csv_persistence_mc_particles,
    "event_info": csv_persistence_event_info,
}
