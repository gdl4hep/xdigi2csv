"""Package that defines variables used within this repository.
:py:mod:`definitions.dmoore` is not imported in order not to have to import
Moore.
"""
from . import dgen

__all__ = ["dgen"]
