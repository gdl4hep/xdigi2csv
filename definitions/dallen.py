"""Variables used for the Allen algorithms.
"""

#: List of the possible Allen algorithms
all_mdf2csv_algos = [
    "hits_velo",
    "hits_ut",
    "hits_scifi",
    "mc_particles",
    "mc_vertices",
    "mc_hit_links",
]


allen_programs = [
    "mdf2csv",
    "velo_validation_no_gec",
    "persistence_velo_tracks",
    "persistence_seeding_tracks",
    "persistence_seeding_xz_tracks",
    "seeding_validation_no_gec",
]

allen_parameters = [
    "number_of_events",
    "number_of_slices",
    "number_of_slices",
    "threads",
    "repetitions",
    "memory",
]
