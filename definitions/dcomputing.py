"""Some variables used for Ganga and HTCondor integration."""

#: Associates a "backend" with the environment variables to pass to the backend
environment_variables_to_copy = {
    "default": ["XDIGI2CSV_REPO", "X509_USER_PROXY", "HOME"],
    "ganga-grid": ["XDIGI2CSV_REPO"],  # only `XDIGI2CSV_REPO``
    "ganga-local": ["XDIGI2CSV_REPO"]  # only `XDIGI2CSV_REPO``
}

#: List of the ganga backends
ganga_backends = ["ganga-local", "ganga-dirac"]

#: List of all the backends
backends = ["local", "condor"] + ganga_backends
