"""Module that defines the paths to the scripts."""

import os

generic_moore_input = os.path.join("{repo}", "run", "moore", "moore_input.py")
moore_xdigi2csv_script = os.path.join("{repo}", "run", "xdigi2csv", "dump_csv.py")

program_script_path = os.path.join("{repo}", "run", "{program}", "run.py")
moore_program_path = os.path.join("{repo}", "run", "{program}", "moore_program.py")
