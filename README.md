# (X)DIGI2CSV: persist hits and MC particles in a CSV file

The (X)DIGI2CSV repository is a versatile tool for running Allen and Moore algorithms
in a reproducible manner. While its primary function is to convert (X)DIGI files
on the grid into CSV or PARQUET formats, it can also be used for a variety of other
tasks.
For example, it can convert (X)DIGI files into MDF or ROOT, and MDF files into CSV.
The repository also makes it easy to process large sets of files in parallel
by quickly submitting jobs to HTCONDOR.

**Please note that the (X)DIGI2CSV repository was developed primarily for internal use**
**and may have limitations. We do not recommend its use by external parties.**

The documentation can be found at https://xdigi2csv.docs.cern.ch/master.
To download the simulated CSV-like files, please refer to
[this page](https://xdigi2csv.docs.cern.ch/master/Access/2.download_csv.html).

## Why

The (X)DIGI2CSV repository is being developed as part of the ANN4EUROPE project
to train neural networks for tracking at LHCb.
To achieve this goal, we require large amounts of LHCb hits data in a format
that is fast, lightweight, and easy to read.
Specifically, the repository provides tools to
- convert (X)DIGI files on the grid into CSV or PARQUET formats
- submit jobs to HTCondor for parallel processing,
- maintain a log system to check for job failures,
- generate metadata files containing information about the source of the
outputted CSV-like files.

The XDIGI2CSV repository provides a convenient way of running Moore algorithms,
and overcomes several limitations that arise when using Python files to configure
the input of the algorithms. These include:
- Relative paths are not well-defined by default and can cause issues when specifying
input and output files, as they are defined with respect to the working directory.
- Job splitting and submission can be challenging to put in place
- Generating PFNs and XML catalogs manually is time-consuming, especially when
dealing with hundreds of files.
- The use of Python files to define the input of the algorithms is not modular
and can be difficult to customize or reuse.


It's worth noting that the (X)DIGI2CSV repository was initially developed as an API
to run algorithms in the LHCb stack, mainly for converting XDIGI files into CSV files.
Additional algorithms such as XDIGI2MDF, XDIGI2ROOT, and MDF2ROOT were subsequently
added to the API due to their usefulness.


## How

By using the (X)DIGI2CSV repository, you can quickly and quite easily convert XDIGI files
on the grid into CSV-like formats, e.g., lz4-compressed parquet files.
All you need to do is define a YAML configuration file (e.g., named `xdigi2csv.yaml`)
with the following contents:
```yaml
moore_input:
  nb_files: 3
  start_index: 0
  # Input file is in the grid
  bookkeeping_path: "/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/30000000/XDIGI"
  dddb_tag: "dddb-20210617"
  conddb_tag: "sim-20210617-vc-md100"
  evt_max: -1
xdigi2csv:  
  detectors: # detector hits or clusters to dump
  - velo
  - ut
  - scifi
  # Extension and compression
  format: parquet
  compression: lz4
output:
  outdir: "minbias-sim10aU1" # output directory of the CSV files
computing:
  program: xdigi2csv # program to run
```
The command `./run/run.py -c xdigi2csv.yaml` then runs the conversion.
If you want to run multiple jobs using HTCONDOR,
you can include `backend: condor` and `nb_files_per_job: ...` in the `computing` section
of the YAML file.
Additionally, other programs such as `XDIGI2ROOT`, `XDIGI2MDF`, and `MDF2ROOT`
can be run using this repository.

We highly recommend reading the [the setup guide](https://xdigi2csv.docs.cern.ch/master/Guide/1.setup.html)
for detailed instructions on how to set up this repository.
It's important to note that the XDIGI2CSV program is not currently merged
into the master branch of the LHCb stack
(see [Allen!1125](https://gitlab.cern.ch/lhcb/Allen/-/merge_requests/1125)).
Therefore, you'll need to set up a working
LHCb stack on the appropriate branches to use these programs.
If you don't have access to a compatible stack,
you can use the one we have provided on a LXPLUS machine.

## Organisation of the repository

The repository is organized as follows:

- `data` contains local MDF and CSV files. Larger input files are stored in
`/eos/lhcb/user/a/anthonyc/tracking/data`.
- `run` contains helper scripts to run the Allen and Moore algorithms
- `jobs` contains YAML files used to generate the CSV files that are in the EOS,
as well as other examples
- `python` contains notebooks to analyse the outputs of the algorithms
- `readme` contains README markdown files, also used in the documentation.
- `docs` contains the source of the sphinx-generated documentation.
