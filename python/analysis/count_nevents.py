#!/usr/bin/env python3
"""Count the number of events of a given production.
"""

from __future__ import annotations
import typing
import os
import argparse
import yaml
import tqdm
from concurrent import futures
import numpy as np
from pyarrow.lib import ArrowInvalid
import mplhep
from matplotlib import rcParams
import matplotlib.pyplot as plt
from definitions import dconversion
from tools.tpaths import write_yaml_file


# Style for the plot
mplhep.style.use(mplhep.style.LHCb2)
rcParams.update({"font.serif": "DejaVu Serif"})

#: Name of the file that is used to count the number of events
USED_ALGONAME = "mc_particles"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Count the number of events in a given production"
    )
    parser.add_argument(
        "-i", "--indir", help="Input directory where the subdirectories of data are"
    )
    parser.add_argument(
        "-o",
        "--outpath",
        help="Path where to save the YAML file that shows the number of events",
    )
    parser.add_argument(
        "-p",
        "--plotpath",
        help="Path where to save the plot that shows the number of events / file",
    )
    parser.add_argument(
        "-f",
        "--format",
        help="Format of the CSV-like files",
        choices=dconversion.formats,
        default="csv",
    )
    parser.add_argument(
        "-c",
        "--compression",
        help="Compression used in the CSV-like files",
        default=None,
    )
    parser.add_argument(
        "-n",
        "--ncores",
        help="Number of cores to use in the loop",
        type=int,
        default=1,
    )
    parsed_args = parser.parse_args()
    indir = parsed_args.indir
    outpath = parsed_args.outpath
    plotpath = parsed_args.plotpath
    ncores = parsed_args.ncores
    format = parsed_args.format
    compression = parsed_args.compression
    if compression == "None":
        compression = None

    subdirs = [
        file_or_dir.name for file_or_dir in os.scandir(indir) if file_or_dir.is_dir()
    ]
    ext = dconversion.get_extension(format=format, compression=compression)

    #: List of existing CSV files that cannot be read
    broken = []
    #: Array of number of events (1 entry per sub-directory)
    array_nb_events = []

    def count_events(subdir: str) -> typing.Tuple[int | None, str]:
        """Count the number of events in the CSV-like file :py:data:`USED_ALGONAME`
        stored in the ``subdir`` folder.

        Args:
            subdir: sub-directory inside ``indir``

        Returns:
            A tuple whose first element is the number of events in the CSV-like file,
            and the second is ``subdir`` passed along. The number of events is set
            to ``None`` if the CSV file exists but could not be read.
        """
        path = os.path.join(indir, subdir, USED_ALGONAME + ext)
        if os.path.exists(path):
            read = dconversion.get_io_function(action="r", format=format)
            if format == "csv":
                import pyarrow.csv as pac

                read_kwargs = dict(
                    convert_options=pac.ConvertOptions(include_columns=["event"])
                )
            else:
                read_kwargs = dict(columns=["event"])
            try:
                table = read(path, **read_kwargs)
            except ArrowInvalid:
                return None, subdir
            return len(np.unique(table["event"].to_numpy())), subdir
        else:
            return 0, subdir

    with futures.ProcessPoolExecutor(max_workers=ncores) as pool:
        for nb_events, subdir in tqdm.tqdm(
            pool.map(count_events, subdirs), total=len(subdirs)
        ):
            if nb_events is None:
                broken.append(subdir)
            else:
                array_nb_events.append(nb_events)

    nb_events_status = {
        "counts": {
            "files": len(subdirs),
            "events": sum(array_nb_events),
            "broken": len(broken),
        },
        "broken": broken,
    }
    print(yaml.dump(nb_events_status, sort_keys=False))
    write_yaml_file(nb_events_status, outpath)
    fig, ax = plt.subplots(figsize=(8, 6))
    ax.hist(array_nb_events)
    ax.set_xlabel("# events")
    ax.set_ylabel("# files")
    fig.tight_layout()
    fig.savefig(plotpath, dpi=300, bbox_inches="tight")
    print("Plot saved in", plotpath)
