#!/usr/bin/env python3
"""Check the ``log.yaml`` files of a given Moore production, and count the number of:

* Missing logs: no ``log.yaml`` files
* Successful logs: ``log.yaml`` with a ``returncode`` different equal to 0
* Failing logs: ``log.yaml`` with a ``returncode`` equal equal to 0
* Logs of production with no input: ``returncode`` is -1

The output is saved in a YAML file
"""
from __future__ import annotations
import typing
import os
import argparse
import yaml
import tqdm
from concurrent import futures
from tools.tpaths import write_yaml_file


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Check the `log.yaml` in the sub-directories of a given production"
    )
    parser.add_argument(
        "-i", "--indir", help="Input directory where the subdirectories of data are"
    )
    parser.add_argument("-o", "--outpath", help="Path where to save the YAML file")
    parser.add_argument(
        "-n",
        "--ncores",
        help="Number of cores to use in the loop",
        type=int,
        default=1,
    )
    parsed_args = parser.parse_args()
    indir = parsed_args.indir
    outpath = parsed_args.outpath
    ncores = parsed_args.ncores

    #: Subdirectories with missing log
    logs_missing = []
    #: Subdirectories with log wit return code different from 0
    logs_failed = []
    #: Subdirectories with successful log
    logs_successful = []
    #: Subdirectories with no CSV-like files
    logs_no_input = []

    subdirs = [
        file_or_dir.name for file_or_dir in os.scandir(indir) if file_or_dir.is_dir()
    ]
    print("Number of sub-directories:", len(subdirs))

    def log_return_code(subdir: str) -> typing.Tuple[str, int | None]:
        """Given the sub-directory the log is, add the log into the lists
        ``logs_missing``, ``logs_failed``, or ``log_successful`` according to
        the state of the log.

        Args:
            subdir: the name of the subdirectory the log is in, inside the ``indir``
                folder
        """
        log_path = os.path.join(indir, subdir, "log.yaml")
        if os.path.exists(log_path):
            with open(log_path, "r") as log_file:
                log = yaml.safe_load(log_file)
            if log is None:
                return subdir, -2
            else:
                return subdir, log["returncode"]
        else:
            return subdir, None

    print("Classifying log")
    with futures.ProcessPoolExecutor(max_workers=ncores) as pool:
        for subdir, returncode in tqdm.tqdm(
            pool.map(log_return_code, subdirs), total=len(subdirs)
        ):
            if returncode is None or returncode == -2:
                logs_missing.append(subdir)
            elif returncode == 0:
                logs_successful.append(subdir)
            elif returncode == -1:
                logs_no_input.append(subdir)
            else:
                logs_failed.append(subdir)

    log_status = {
        "counts": dict(
            successful=len(logs_successful),
            failed=len(logs_failed),
            missing=len(logs_missing),
            no_input=len(logs_no_input),
        ),
        "errors": dict(
            failed=logs_failed, missing=logs_missing, no_input=logs_no_input
        ),
    }

    print("Log status:")
    print(yaml.dump(log_status, sort_keys=False))
    write_yaml_file(log_status, outpath)
